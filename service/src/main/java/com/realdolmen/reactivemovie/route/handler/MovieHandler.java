package com.realdolmen.reactivemovie.route.handler;

import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

public interface MovieHandler {
    Mono<ServerResponse> create(ServerRequest request);

    Mono<ServerResponse> findAll(ServerRequest request);

    Mono<ServerResponse> findById(ServerRequest request);

    Mono<ServerResponse> findByIdVariation(ServerRequest request);

    Mono<ServerResponse> findByTitle(ServerRequest request);
}
