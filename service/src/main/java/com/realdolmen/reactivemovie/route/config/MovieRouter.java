package com.realdolmen.reactivemovie.route.config;

import com.realdolmen.reactivemovie.model.Movie;
import com.realdolmen.reactivemovie.route.filter.LogFilter;
import com.realdolmen.reactivemovie.route.handler.MovieHandler;
import com.realdolmen.reactivemovie.service.MovieService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

import static org.springframework.web.reactive.function.server.RequestPredicates.GET;
import static org.springframework.web.reactive.function.server.RequestPredicates.accept;
import static org.springframework.web.reactive.function.server.RouterFunctions.route;
import static org.springframework.web.reactive.function.server.ServerResponse.ok;

@Configuration
public class MovieRouter {
    public static final String REACTIVE_MOVIE_ENDPOINT = "/reactive/movie";
    public static final String REACTIVE_MOVIE_ALTERNATIVE_ENDPOINT = "/reactive/movie-alternative";

    @Bean
    public RouterFunction<ServerResponse> movieRoutes(MovieService service) {
        return route(GET(REACTIVE_MOVIE_ENDPOINT),
                req -> ok().body(
                        service.findAll(), Movie.class))

                .and(route(GET(REACTIVE_MOVIE_ENDPOINT + "/{id}"),
                        req -> Mono.justOrEmpty(req.pathVariable("id"))
                                .map(Long::valueOf)
                                .flatMap(id -> ok().body(service.findById(Mono.just(id)), Movie.class))
                ));
    }

    @Bean
    public RouterFunction<ServerResponse> movieRoutesWithHandlers(MovieHandler handler) {
        return route()
                .GET(REACTIVE_MOVIE_ALTERNATIVE_ENDPOINT, accept(MediaType.APPLICATION_JSON), handler::findAll).filter(new LogFilter())
                .GET(REACTIVE_MOVIE_ALTERNATIVE_ENDPOINT + "/{id}", accept(MediaType.APPLICATION_JSON), handler::findById)
                .GET(REACTIVE_MOVIE_ALTERNATIVE_ENDPOINT + "/variation/{id}", accept(MediaType.APPLICATION_JSON), handler::findByIdVariation)
                .GET(REACTIVE_MOVIE_ALTERNATIVE_ENDPOINT + "/title/{title}", accept(MediaType.APPLICATION_JSON), handler::findByTitle)
                .POST(REACTIVE_MOVIE_ALTERNATIVE_ENDPOINT, accept(MediaType.APPLICATION_JSON), handler::create)
                .build();
    }
}
