package com.realdolmen.reactivemovie.route.handler;

import com.realdolmen.reactivemovie.model.Movie;
import com.realdolmen.reactivemovie.service.MovieService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.web.reactive.function.BodyInserters.fromObject;
import static org.springframework.web.reactive.function.server.ServerResponse.*;

@Slf4j
@Component
@AllArgsConstructor
public class MovieHandlerImpl implements MovieHandler {
    private final MovieService movieService;
    private final RequestHandler requestHandler;

    @Override
    public Mono<ServerResponse> create(ServerRequest request) {
        //Example without validation
        /*Mono<Movie> movieMono = request.bodyToMono(Movie.class);
        return status(CREATED)
                .contentType(MediaType.APPLICATION_JSON)
                .body(movieService.createWithPublisher(movieMono), Void.class);*/

        return requestHandler.requireValidBody(
                movie -> status(CREATED)
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(movieService.createWithPublisher(movie), Void.class),
                request,
                Movie.class);
    }

    /*@Override
    public Mono<ServerResponse> create(ServerRequest request) {
        return request.bodyToMono(Movie.class)
                .flatMap(movieService::createWithPublisher)
                .flatMap(movie -> status(CREATED).body(movie, Movie.class));
    }*/

    @Override
    public Mono<ServerResponse> findAll(ServerRequest request) {
        Flux<Movie> movies = movieService.findAll();
        return ok()
                .contentType(MediaType.APPLICATION_JSON)
                .body(movies, Movie.class);
    }

    @Override
    public Mono<ServerResponse> findById(ServerRequest request) {
        return Mono.justOrEmpty(request.pathVariable("id"))
                .map(Long::valueOf)
                .flatMap(id -> ok()
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(movieService.findById(Mono.just(id)), Movie.class))
                .switchIfEmpty(notFound().build());
    }

    @Override
    public Mono<ServerResponse> findByIdVariation(ServerRequest request) {
        Long id = Long.valueOf(request.pathVariable("id"));
        return movieService.findById(Mono.just(id))
                .flatMap(movie -> ok()
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(fromObject(movie)))
                .switchIfEmpty(notFound().build());
    }

    @Override
    public Mono<ServerResponse> findByTitle(ServerRequest request) {
        return Mono.justOrEmpty(request.pathVariable("title"))
                .map(String::valueOf)
                .flatMap(title -> ok()
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(movieService.findByTitle(title), Movie.class)) //alternative
//                        .body(fromObject(movieService.findByTitle(title))))
                .switchIfEmpty(notFound().build());
    }
}
