package com.realdolmen.reactivemovie;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ReactiveMovieApplication {

    public static void main(String[] args) {
        SpringApplication.run(ReactiveMovieApplication.class, args);
    }
}
