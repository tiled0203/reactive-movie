package com.realdolmen.reactivemovie.model;

import lombok.*;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotBlank;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@NoArgsConstructor
@Document
public class Director extends BaseEntity {
    @NonNull
    @NotBlank
    private String firstName;
    @NonNull
    @NotBlank
    private String lastName;

    @lombok.Builder(builderClassName = "Builder", toBuilder = true)
    public Director(Long id, @NonNull String firstName, @NonNull String lastName) {
        super(id);
        this.firstName = firstName;
        this.lastName = lastName;
    }
}
