package com.realdolmen.reactivemovie.service;

import com.realdolmen.reactivemovie.model.Movie;
import com.realdolmen.reactivemovie.repository.MovieRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.reactivestreams.Publisher;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Slf4j
@Service
@AllArgsConstructor
public class MovieServiceImpl implements MovieService {
    private final MovieRepository repository;

    @Override
    public Flux<Movie> findByTitle(String title) {
        return repository.findByTitle(title);
    }

    @Override
    public Mono<Movie> findById(Publisher<Long> id) {
        return repository.findById(id);
    }

    @Override
    public Flux<Movie> findAll() {
        return repository.findAll();
    }

    @Override
    public Mono<Movie> update(Movie movie) {
        return repository.save(movie);
    }

    @Override
    public Mono<Movie> create(Mono<Movie> moviePublisher) {
        return repository.save(moviePublisher.block());
    }

    @Override
    public Mono<Void> createWithPublisher(Publisher<Movie> moviePublisher) {
        return repository.saveAll(moviePublisher).then();
    }
}
