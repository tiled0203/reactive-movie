package com.realdolmen.reactivemovie.service;

import com.realdolmen.reactivemovie.model.Movie;
import org.reactivestreams.Publisher;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface MovieService {
    Flux<Movie> findByTitle(String title);

    Mono<Movie> findById(Publisher<Long> id);

    Flux<Movie> findAll();

    Mono<Movie> update(Movie movie);

    Mono<Movie> create(Mono<Movie> moviePublisher);

    Mono<Void> createWithPublisher(Publisher<Movie> moviePublisher);
}
