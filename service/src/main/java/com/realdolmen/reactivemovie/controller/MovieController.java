package com.realdolmen.reactivemovie.controller;

import com.realdolmen.reactivemovie.model.Movie;
import com.realdolmen.reactivemovie.service.MovieService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

@Slf4j
@Api(value = "Rest Controller for querying movies")
@RestController
@RequestMapping("/movie")
@AllArgsConstructor
public class MovieController {
    private final MovieService service;

    @GetMapping
    @ApiOperation(value = "Find all movies ever made", notes = "Should find out how to limit stream")
    public Flux<Movie> getAll() {
        return service.findAll();
    }
}
