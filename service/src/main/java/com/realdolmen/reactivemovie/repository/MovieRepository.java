package com.realdolmen.reactivemovie.repository;

import com.realdolmen.reactivemovie.model.Movie;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

@Repository
public interface MovieRepository extends ReactiveMongoRepository<Movie, Long> {
    Flux<Movie> findByTitle(String title);
}
