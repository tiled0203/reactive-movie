package com.realdolmen.reactivemovie.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MovieDto {
    private String title;
    private String description;
    private LocalDate releaseDate;
    private List<CategoryDto> categories;
    private DirectorDto director;
}
