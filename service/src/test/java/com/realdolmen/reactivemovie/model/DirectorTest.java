package com.realdolmen.reactivemovie.model;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.assertNotNull;

public class DirectorTest {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void testBuilderSuccess() {
        Director director = Director.builder()
                .firstName("john")
                .lastName("Doe")
                .build();

        assertNotNull(director);
    }

    @Test
    public void testBuilderMissingFirstName() {
        expectedException.expect(NullPointerException.class);

        Director.builder().lastName("Doe").build();
    }

    @Test
    public void testBuilderMissingLastName() {
        expectedException.expect(NullPointerException.class);

        Director.builder().firstName("John").build();
    }
}