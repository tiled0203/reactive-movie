package com.realdolmen.reactivemovie;

import com.realdolmen.reactivemovie.repository.MovieRepository;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public abstract class AbstractServiceTest {
    @Autowired
    protected WebTestClient webTestClient;
    @Autowired
    protected MovieRepository movieRepository;

    @Before
    public void setUp() throws Exception {
        movieRepository.deleteAll().block();
    }
}
